import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productsRespository = AppDataSource.getRepository(Product)

    const products = await productsRespository.find()
    console.log("Loaded products: ", products)

    const updateProduct = await productsRespository.findOneBy({id: 1})
    console.log(updateProduct)
    updateProduct.price=80000
    await productsRespository.save(updateProduct)

}).catch(error => console.log(error))
